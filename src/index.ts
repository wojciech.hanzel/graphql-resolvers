import { ApolloServer, gql } from "apollo-server";
import { readFileSync } from "fs";

import resolvers from "./resolvers";
import schema from "./schema";

async function mainSDL() {
  const typeDefs = gql(
    readFileSync(__dirname + "/schema.gql", {
      encoding: "UTF-8",
    }),
  );

  const server = new ApolloServer({
    typeDefs,
    resolvers,
    context: {
      user: { id: "1" },
    },
    playground: true,
  });

  await server.listen(4000);
  console.log("Server SDL listening on 4000!");
}

async function mainJS() {
  const server = new ApolloServer({
    schema,
    playground: true,
  });

  await server.listen(4001);
  console.log("Server JS listening on 4001!");
}

mainSDL();
mainJS();
